from flask_openipa.exceptions import MissingRequirementException
from flask_openipa.schemas.plugins.base import BaseSchemaPlugin

try:
    from pydantic import BaseModel, schema_of
except ImportError as error:
    raise MissingRequirementException(
        "To use marshmallow plugin you need to install marshmallow extras. (pip install flask-openipa[marshmallow])"
    )


class PydanticSchemaPlugin(BaseSchemaPlugin):
    def __init__(self, openapi_version: str = "3.0.0"):
        super().__init__(openapi_version)

    def add_schema(self, schema: BaseModel) -> dict:
        result = schema_of(schema, ref_template=self.schemas_path)

        for name, schema in result["definitions"].items():
            self.new_schema_handler(name, schema)

        if result.get("type") == "array":
            {"type": "array", "$ref": result["items"]}
        else:
            return {"type": "object", "$ref": result["$ref"]}
