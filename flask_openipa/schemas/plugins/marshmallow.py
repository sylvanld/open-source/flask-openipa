from typing import Type, Union

from flask_openipa.exceptions import MissingRequirementException
from flask_openipa.schemas.plugins.base import BaseSchemaPlugin

try:
    from apispec import APISpec
    from apispec.exceptions import DuplicateComponentNameError
    from apispec.ext.marshmallow import MarshmallowPlugin
    from marshmallow import Schema
except ImportError as error:
    raise MissingRequirementException(
        "To use marshmallow plugin you need to install marshmallow extras. (pip install flask-openipa[marshmallow])"
    )


def generate_schema_name(schema: Union[Schema, Type[Schema]]):
    if isinstance(schema, Schema):
        return schema.__class__.__name__
    elif isinstance(schema, type) and issubclass(schema, Schema):
        return schema.__name__
    else:
        raise NotImplementedError


class MarshmallowSchemaPlugin(BaseSchemaPlugin):
    def __init__(self, openapi_version: str = "3.0.0"):
        super().__init__(openapi_version)

    @property
    def __apispec(self) -> APISpec:
        if not hasattr(self, "spec"):
            setattr(
                self,
                "spec",
                APISpec(
                    "mock-apispec",
                    "0.0.1",
                    self.openapi_version,
                    plugins=(MarshmallowPlugin(),),
                ),
            )
        return getattr(self, "spec")

    def add_schema(self, schema: Schema) -> dict:
        schema_name = generate_schema_name(schema)
        schema_ref = self.schemas_path.format(model=schema_name)

        try:
            result = self.__apispec.components.schema(schema_name, schema=schema)
            for schema_name, schema_data in result.schemas.items():
                self.new_schema_handler(schema_name, schema_data)
        except DuplicateComponentNameError:
            # if schema is already added, no need to add it...
            pass

        if getattr(schema, "many", False):
            return {"type": "array", "items": {"$ref": schema_ref}}
        else:
            return {"type": "object", "$ref": schema_ref}
