import abc
from typing import Any, Callable


def empty_new_schema_handler(name: str, schema: dict):
    print("Schema registered", name, schema)


class BaseSchemaPlugin(abc.ABC):
    def __init__(
        self, openapi_version: str = "3.0.0",
    ):
        self.openapi_version = openapi_version
        self.new_schema_handler = empty_new_schema_handler
        self.schemas_path = "#/components/schemas/{model}"

    def on_new_schema(self, new_schema_handler: Callable[[str, dict], None] = None):
        self.new_schema_handler = new_schema_handler

    @abc.abstractmethod
    def add_schema(self, schema: Any) -> dict:
        raise NotImplementedError
