# Quickstart

## Configure extension with plugins

```python
from flask_openipa import FlaskOpenIPA
from flask_openipa.schemas.plugins.marshmallow import MarshmallowSchemaPlugin
from flask_openipa.schemas.plugins.pydantic import PydanticSchemaPlugin

openapi = FlaskOpenIPA(
    plugins=(MarshmallowSchemaPlugin(), PydanticSchemaPlugin())
)
```

There are 2 schemas plugins shipped with this package to generate schemas from [marshmallow]() or [pydantic]() objects.

By default their dependencies are not installed. You can add both by installing `flask-openipa[pydantic,marshmallow]`

## Register extension in your app

```python
openapi.init_app(app)
```

## Document flask views

```python
from myapp.extensions import openapi

@blueprint.route()
@openapi.spec()
def my_flask_view():
    ...
```

## Generate OpenAPI specs

* You can configure extension to serve your spec at a given path of your app

```python
class Config:
    API_SWAGGER_PATH = "/docs/" # this one is optional and requires swagger-ui extension
    API_SPEC_URL = "/docs/openapi.json"

app.config.from_object(Config)
```

To expose it using an embed swagger UI, you need to configure `API_SWAGGER_URL`. This requires swagger-ui extras to be installed using `pip install flask-openipa[swagger-ui]`.

* If you'd rather get it manually from your python code, you can use:

```python
spec_dict = openapi.openapi_spec.to_dict()
```
